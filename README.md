# corgime

Hubot coffee script to return cat images

## Installation

Run the following command 

    $ npm install hubot-corgime

Then to make sure the dependencies are installed:

    $ npm install

To enable the script, add a `hubot-corgime` entry to the `external-scripts.json`
file (you may need to create this file).

    ["hubot-corgime"]
